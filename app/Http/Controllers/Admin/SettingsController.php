<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use App\User;

class SettingsController extends Controller {
    
    public function getSettings() {        

        return view('admin/settings');
        
    }
    
    public function setSettings(Request $request) {
        
        $updatePurchasing = User::where('id', '>', 0)->update(['purchasing' => $request->purchasing]);
        
        if($updatePurchasing) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

}

