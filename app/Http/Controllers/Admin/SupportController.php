<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use App\User;
use App\Support;

class SupportController extends Controller {
    
    public function getSupport() {

        $supports = DB::table('support')
            ->join('users2s', 'users2s.id', '=', 'support.user_id')
            ->select('support.*', 'users2s.username', 'users2s.country')
            ->where('support.owner', 1)
            ->orderBy('support.id', 'desc')
            ->paginate(50);

        return view('admin/support', compact('supports'));
        
    }

    public function setDeleteSupport($id) {

        $del = Support::where('id', $id)->update(['is_viewed' => 1]);

        if($del) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

}

