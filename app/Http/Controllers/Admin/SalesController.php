<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use Carbon;
use App\User;
use App\Clicks;
use App\Transactions;
use App\Notifications;
use App\PremiumSales;

class SalesController extends Controller {
    
    public function getSales() {        

        $sales = DB::table('premium_sales')
            ->leftjoin('users2s', 'premium_sales.user_id', '=', 'users2s.id')
            ->select('users2s.username', 'premium_sales.*')
            ->orderBy('premium_sales.id', 'desc')
            ->paginate(50);
        
        return view('admin/sales', compact('sales'));
        
    }

    public function setCancelSale($id) {        

        $sale = PremiumSales::where('id', $id)->first();

        $deleteSale = PremiumSales::where('id', $id)->delete();

        $updateUser = User::where('id', $sale->user_id)->update(['premium_membership' => null, 'premium_expiration' => null]);

        $updateTransaction = Transactions::where('user_id', $sale->user_id)->orderBy('id', 'desc')->first();
        $updateTransaction->status = 2;
        $updateTransaction->save();

        if($deleteSale) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }

    }
    
}

