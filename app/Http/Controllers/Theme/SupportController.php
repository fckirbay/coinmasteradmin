<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Validator;
use Mail;
use Alert;
use Carbon;
use Sentinel;
use App\User;
use App\Support;

class SupportController extends Controller {

    public function getSupport() {

        $messages = Support::where('user_id', Sentinel::getUser()->id)->where('validity_date', null)->orwhere('user_id', Sentinel::getUser()->id)->where('validity_date', '>=', Carbon\Carbon::now())->orderBy('id', 'desc')->limit(30)->get();

        $checkUnreaded = Support::where('user_id', Sentinel::getUser()->id)->where('owner', 2)->where('is_viewed', 0)->update(['is_viewed' => 1]);
        
        return view('cardgame/support', compact('messages'));
        
    }
    
    public function setSupport(Request $request) {
        
        
        $validator = Validator::make($request->all(), [
            'message' => 'max:300',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $check = Support::where('user_id', Sentinel::getUser()->id)->where('message', $request->message)->count();
        if($check > 0) {
            Alert::error('Error', 'Error')->autoclose(1000);
            return Redirect::back();
        }
        

        $newSupport = new Support();
        $newSupport->user_id = Sentinel::getUser()->id;
        $newSupport->owner = 1;
        $newSupport->message = $request->message;
        $newSupport->date = Carbon\Carbon::now();
        $newSupport->save();

        //Alert::error(\Lang::get('general.your_tc_no_not_found'), \Lang::get('general.sorry'))->autoclose(3000);
        Alert::success(\Lang::get('general.your_message_sent'), \Lang::get('general.congratulations'))->autoclose(1000);
        return Redirect::back();
    }
    

}