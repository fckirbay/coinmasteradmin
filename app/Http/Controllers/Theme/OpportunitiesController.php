<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;

class OpportunitiesController extends Controller {

    public function getOpportunities() {
                
        return view('cardgame/opportunities');

    }

}