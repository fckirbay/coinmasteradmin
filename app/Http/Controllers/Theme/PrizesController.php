<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use App\Blog;
use App\PaymentMethods;
use Alert;

class PrizesController extends Controller {

    public function getPrizes() {

    	$paymentMethods = PaymentMethods::orderBy('id', 'asc')->get();
                
        return view('cardgame/prizes', compact('paymentMethods'));

    }

    public function getPrize($name) {

    	$method = PaymentMethods::where('url', $name)->first();
                
        return view('cardgame/prize', compact('method'));

    }
    
    public function setPrize(Request $request) {

    	Alert::error(\Lang::get('general.your_balance_is_insufficient'), \Lang::get('general.sorry'))->autoclose(2500);
        return Redirect::back();

    }
    
}