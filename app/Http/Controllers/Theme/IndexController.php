<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use DB;
use Carbon;
use Sentinel;
use App\Blog;
use App\Slider;
use App\Rooms;
use App\Boxes;
use App\Prizes;
use App\User;
use App\Notifications;
use App\PremiumSales;
use App\Transactions;
use App\ClickRewarded;
use App\ScratchWin;
use App\Offerwall;

class IndexController extends Controller {

    public function getIndex() {

        if(!Sentinel::check() && session('walkthrough') != 1) {
            session(['walkthrough' => 1]);
            return view('mobile/walkthrough');
        }
        
        $slider = Slider::orderBy('order', 'asc')->get();
        
        if(session('lang') == "tr") {
            $rooms = Rooms::whereIn('location', ['eu', 'wo'])->where('status', 1)->limit(9)->inRandomOrder()->get();
            //$rooms = Rooms::where('location', 'tr')->limit(15)->inRandomOrder()->get();
        } else {
            $rooms = Rooms::whereIn('location', ['eu', 'wo'])->where('status', 1)->limit(9)->inRandomOrder()->get();
        }
        
        $prizes = DB::table('prizes')
            ->leftjoin('users', 'prizes.user_id', '=', 'users.id')
            ->select('users.first_name', 'users.currency', 'prizes.*')
            ->orderBy('prizes.id', 'desc')
            ->limit(25)
            ->get();

        $completedTasks = DB::table('offerwall')
            ->leftjoin('users', 'offerwall.user_id', '=', 'users.id')
            ->select('users.first_name', 'users.currency', 'offerwall.*')
            ->orderBy('offerwall.id', 'desc')
            ->limit(25)
            ->get();

        $earnings = User::where('id', '>', 0)->sum('earnings');
        
        return view('cardgame/index', compact('slider', 'rooms', 'prizes', 'completedTasks', 'earnings'));

    }

    public function getWalkthrough() {
        
        return view('mobile/walkthrough');

    }
    
    public function getPrivacyPolicy() {
        
        return view('mobile/privacy-policy');

    }
    
    public function getUpdate() {
        
        return view('mobile/update');

    }
    
    public function getTurkeyRooms(Request $request) {
        
        $lang = session('lang');
        
        if($lang == "tr") {
            $rooms = Rooms::where('location', 'tr')->where('status', 1)->orderBy('name', 'asc')->get();
        } else {
            $rooms = Rooms::where('location', 'tr')->where('status', 1)->orderBy('name_en', 'asc')->get();
        }
        
        return view('mobile/rooms', compact('rooms', 'lang'));

    }
    
    public function getAbroadRooms(Request $request) {
        
        $lang = session('lang');
        
        if($lang == "tr") {
            $rooms = Rooms::where('location', 'wo')->where('status', 1)->orderBy('name', 'asc')->get();
        } else {
            $rooms = Rooms::where('location', 'wo')->where('status', 1)->orderBy('name_en', 'asc')->get();
        }

        return view('mobile/rooms', compact('rooms', 'lang'));

    }
    
    public function getEuropeRooms(Request $request) {
        
        $lang = session('lang');
        
        if($lang == "tr") {
            $rooms = Rooms::where('location', 'eu')->where('status', 1)->orderBy('name', 'asc')->get();
        } else {
            $rooms = Rooms::where('location', 'eu')->where('status', 1)->orderBy('name_en', 'asc')->get();
        }
        
        return view('mobile/rooms', compact('rooms', 'lang'));

    }

    public function setFirebase(Request $request) {
        
        /*$delete_other_firebases = User::where('firebase', $request->firebase_token)->where('id', '!=', Sentinel::getUser()->id)->update(['firebase'=>null]);*/

        $data = [];

        if(!Sentinel::check()) {
            $data['status'] = "no";
            return $data;
        }
        
        if($request->firebase_token != null) {
            $check_firebases = User::where('firebase', $request->firebase_token)->get();
            if(count($check_firebases) >= 3) {
                foreach($check_firebases as $val) {
                    $block = User::where('id', $val->id)->update(['blocked' => 1, 'blocked_reason' => \Lang::get('general.use_3_or_more_accounts')]);
                }
            }
        }
        
        if(Sentinel::getUser()->firebase != $request->firebase_token || Sentinel::getUser()->app_version != $request->app_version) {
            $bul = User::where('id', Sentinel::getUser()->id)->first();
            $bul->firebase = $request->firebase_token;
            $bul->app_version = $request->app_version;
            $bul->os = $request->os;
            $bul->save();

            $data['status'] = "ok";
        }

        return $data;
        
        /*
        if(Sentinel::getUser()->firebase != $request->firebase_token) {
            $bul = User::where('id', Sentinel::getUser()->id)->first();
            $bul->firebase = $request->firebase_token;
            $bul->os = $request->os;
            $bul->save();
        }
        */
        
    }

    public function setPostback(Request $request) {
        
        //http://app.luckybox.fun/postback/?tx_id={transaction_id}&user_id={s1}&point_value={points}&usd_value={payout}&offer_title={vc_title}
        
        $user = User::where('id', $request->user_id)->first();
		
		$checkBefore = Offerwall::where('user_id', $request->user_id)->where('tx_id', $request->tx_id)->count();
		
		if($checkBefore == 0) {
			$offer = new Offerwall();
			$offer->user_id = $request->user_id;
			$offer->tx_id = $request->tx_id;
			$offer->point_value = $request->point_value;
			$offer->usd_value = $request->usd_value;
			$offer->offer_title = $request->offer_title;
			$offer->is_completed = 1;
			$offer->complete_date = Carbon\Carbon::now();
			$offer->save();

			$admins = User::whereIn('id', array(1))->get();
			foreach($admins as $key => $val) {
				$newNotify = new Notifications();
				$newNotify->user_id = $val->id;
				$newNotify->title = "Görev Tamamlandı!";
				$newNotify->notification = '($'. $request->usd_value .') '. $user->first_name .' adlı kullanıcı '. $request->point_value .' bilet ödüllü '. $request->offer_title .' görevini tamamlandı!';
				$newNotify->firebase = $val->firebase;
				$newNotify->status = 0;
				$newNotify->created_at = Carbon\Carbon::now();
				$newNotify->save();
			}

			if($user->firebase != null) {
				$newNotify = new Notifications();
				$newNotify->user_id = $request->user_id;
				$newNotify->title = "LuckyBox";
				if($user->lang == "tr") {
					$newNotify->notification = $request->offer_title ." görevinden ". $request->point_value ." bilet kazandınız!";
				} else {
					$newNotify->notification = "You have won ". $request->point_value ." tickets from the ". $request->offer_title ." task.";
				}
				$newNotify->firebase = $user->firebase;
				$newNotify->status = 0;
				$newNotify->created_at = Carbon\Carbon::now();
				$newNotify->save();
			}

			$givePrize = User::where('id', $request->user_id)->increment('ticket', $request->point_value);

			if($user->reference_id != null) {
			    $reference = User::where('id', $user->reference_id)->first();

                if($reference->firebase != null) {
                    $newNotify = new Notifications();
                    $newNotify->user_id = $reference->id;
                    $newNotify->title = "LuckyBox";
                    if($user->lang == "tr") {
                        $newNotify->notification = $user->id ." nolu referans üyenizin tamamladığı bir görevden ". $request->point_value ." bilet kazandınız.";
                    } else {
                        $newNotify->notification = "You have earned ". $request->point_value." tickets from a task completed by your reference member " .$user->id. ".";
                    }
                    $newNotify->firebase = $reference->firebase;
                    $newNotify->status = 0;
                    $newNotify->created_at = Carbon\Carbon::now();
                    $newNotify->save();

                }
                $reference_winnings = floatval($request->point_value) / 2;
                $givePrize = User::where('id', $user->reference_id)->increment('ticket', $reference_winnings);
            }
		}

        

        echo "Completed";

        //$err->message = $request->tx_id . ' - ' . $request->user_id . ' - ' . $request->point_value . ' - ' . $request->usd_value . ' - ' . $request->offer_title;

    }

    public function setPostbackVideo(Request $request) {
        
        //http://app.luckybox.fun/postback/?tx_id={transaction_id}&user_id={s1}&point_value={points}&usd_value={payout}&offer_title={vc_title}
        
        $user = User::where('id', $request->user_id)->first();
        
        $checkBefore = Offerwall::where('user_id', $request->user_id)->where('tx_id', $request->tx_id)->count();
        
        if($checkBefore == 0) {
            $offer = new Offerwall();
            $offer->user_id = $request->user_id;
            $offer->tx_id = $request->tx_id;
            $offer->point_value = 0;
            $offer->usd_value = 0;
            $offer->offer_title = $request->offer_title;
            $offer->is_completed = 1;
            $offer->complete_date = Carbon\Carbon::now();
            $offer->save();

            $admins = User::whereIn('id', array(1))->get();
            foreach($admins as $key => $val) {
                $newNotify = new Notifications();
                $newNotify->user_id = $request->user_id;
                $newNotify->title = "Görev Tamamlandı!";
                $newNotify->notification = $user->first_name ." adlı kullanıcı ". $request->point_value ." bilet ödüllü bir görevi tamamlandı!";
                $newNotify->firebase = $val->firebase;
                $newNotify->status = 0;
                $newNotify->created_at = Carbon\Carbon::now();
                $newNotify->save();
            }

            if($user->firebase != null) {
                $newNotify = new Notifications();
                $newNotify->user_id = $request->user_id;
                $newNotify->title = "LuckyBox";
                if($user->lang == "tr") {
                    $newNotify->notification = $request->offer_title ." görevinden ". $request->point_value ." bilet kazandınız!";
                } else {
                    $newNotify->notification = "You have won ". $request->point_value ." tickets from the ". $request->offer_title ." task.";
                }
                $newNotify->firebase = $user->firebase;
                $newNotify->status = 0;
                $newNotify->created_at = Carbon\Carbon::now();
                $newNotify->save();
            }

            $givePrize = User::where('id', $request->user_id)->increment('ticket', $request->point_value);
        }

        

        echo "Completed";

        //$err->message = $request->tx_id . ' - ' . $request->user_id . ' - ' . $request->point_value . ' - ' . $request->usd_value . ' - ' . $request->offer_title;

    }

    


    public function setClickRewarded(Request $request) {

        $data = [];
        
        
        $from = Carbon\Carbon::now()->subHours(6);
        $to = Carbon\Carbon::now();

        

        $count = ClickRewarded::where('user_id', Sentinel::getUser()->id)->where('is_completed', 1)->whereBetween('complete_date', array($from, $to))->count();

        $tries = ClickRewarded::where('user_id', Sentinel::getUser()->id)->whereBetween('click_date', array($from, $to))->count();

        if(session('extra', 0) != 0) {
            $total_watches = session('extra');
        } else {
            $total_watches = 5;
        }

        if($tries > 1000) {
            $data['result'] = "1000 defadan fazla denediğiniz için bugün reklam izleyemezsiniz! Uygulamanın son sürümünü yüklediğinizden emin olun! Sorunu çözememeniz halinde destek menüsünden bize yazın!";
            $data['token'] = "";
        } elseif($count < $total_watches) {
            $token = bin2hex(random_bytes(16));        
        
            $newClick = new ClickRewarded();
            $newClick->user_id = Sentinel::getUser()->id;
            $newClick->token = $token;
            $newClick->is_completed = 0;
            $newClick->click_date = Carbon\Carbon::now();
            $newClick->save();

            $data['result'] = "success";
            $data['token'] = $token;
            
        } else {

            $data['result'] = "You can watch ". $total_watches ." ads in 6 hours!";
            $data['token'] = "";
        }

        return $data;
        

    }

    public function setWatchRewarded(Request $request) {

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();

        $checkRewarded = ClickRewarded::where('user_id', $request->user_id)->where('token', $request->token)->where('is_completed', 0)->whereBetween('click_date', array($from, $to))->first();

        if($checkRewarded) {
            $checkRewarded->is_completed = 1;
            $checkRewarded->complete_date = Carbon\Carbon::now();
            $checkRewarded->save();

            $increment = User::where('id', $request->user_id)->increment('ticket', 0.20);

            // Burada eğer son 12 saatte 5. reklam izlemei olduysa bildirim gönderiyoruz...
            $user = User::select('firebase', 'is_notify')->where('id', $request->user_id)->first();
            if($user->is_notify == 1 && $user->firebase != null) {
                $newNotify = new Notifications();
                $newNotify->user_id = $request->user_id;
                $newNotify->title = "LuckyBox News";
                $newNotify->notification = \Lang::get('general.the_award_winning_video_is_ready');
                //"Ödüllü videonuz hazır! Hemen izleyerek hediye biletlerinizi alın.";
                $newNotify->firebase = $user->firebase;
                $newNotify->time = Carbon\Carbon::now()->addHours(6);
                $newNotify->status = 0;
                $newNotify->created_at = Carbon\Carbon::now();
                $newNotify->save();
            }
        } else {
            $checkScratch = ScratchWin::where('user_id', $request->user_id)->where('token', $request->token)->where('is_completed', 0)->whereBetween('click_date', array($from, $to))->first();
            
            if($checkScratch) {
                $checkScratch->is_completed = 1;
                $checkScratch->complete_date = Carbon\Carbon::now();
                $checkScratch->save();

                session(['scratch_token' => $request->token]);

                // Burada eğer son 24 saatte reklam izlemesi olduysa bildirim gönderiyoruz...
                /*
                $user = User::select('firebase', 'is_notify')->where('id', $request->user_id)->first();
                if($user->is_notify == 1) {
                    $newNotify = new Notifications();
                    $newNotify->user_id = $request->user_id;
                    $newNotify->title = "LuckyBox News";
                    $newNotify->notification = \Lang::get('general.the_award_winning_video_is_ready');
                    //"Ödüllü videonuz hazır! Hemen izleyerek hediye biletlerinizi alın.";
                    $newNotify->firebase = $user->firebase;
                    $newNotify->time = Carbon\Carbon::now()->addHours(12);
                    $newNotify->status = 0;
                    $newNotify->created_at = Carbon\Carbon::now();
                    $newNotify->save();
                }
                */
            }
        }

        $response["success"] = true;
        echo json_encode($response);

    }
    
    public function setBuyVip(Request $request) {
		
		$checkBefore = PremiumSales::where('user_id', $request->user_id)->where('expiry_date', '>', Carbon\Carbon::now())->count();
        
		if($checkBefore == 0) {
			$newPayment = new PremiumSales();
			$newPayment->user_id = $request->user_id;
			$newPayment->membership_id = $request->transaction;
			$newPayment->trans_id = $request->trans_id;
			$newPayment->purchase_token = $request->purchase;
			$newPayment->expiry_date = Carbon\Carbon::now()->addDay();
			$newPayment->save();

			if(substr($request->trans_id, 0, 3) == "GPA") {


				if($request->transaction === "bronze_tr" || $request->transaction === "bronze_en") {
					$amount = 5;
				} if($request->transaction === "silver_tr" || $request->transaction === "silver_en") {
					$amount = 10;
				} if($request->transaction === "gold_tr" || $request->transaction === "gold_en") {
					$amount = 20;
				} if($request->transaction === "platinum_tr" || $request->transaction === "platinum_en") {
					$amount = 30;
				} if($request->transaction === "vip_tr" || $request->transaction === "vip_en") {
					$amount = 50;
				}

				$finduser = User::where('id', $request->user_id)->first();
				$finduser->premium_membership = $request->transaction;
				$finduser->premium_expiration = Carbon\Carbon::now()->addDay();
				$finduser->save();

				$admins = User::whereIn('id', array(1))->get();
				foreach($admins as $key => $val) {
					$newNotify = new Notifications();
					$newNotify->user_id = $val->id;
					$newNotify->title = "Yeni Satış!";
					$newNotify->notification = $finduser->first_name ." adlı kullanıcı ". strtoupper($request->transaction) ." üyelik satın aldı. Kendisine teşekkür ediyoruz. :)";
					$newNotify->firebase = $val->firebase;
					$newNotify->status = 0;
					$newNotify->created_at = Carbon\Carbon::now();
					$newNotify->save();
				}

				//$giveTicket = User::where('id', $request->user_id)->increment('ticket', $amount);
				$fromDay = Carbon\Carbon::now()->subHours(24);
				$toDay = Carbon\Carbon::now();
				$videoCompleted = ScratchWin::where('user_id', $finduser->id)->whereIn('is_completed', array(1, 2))->whereBetween('click_date', array($fromDay, $toDay))->update(['click_date' => Carbon\Carbon::now()->subDay(), 'complete_date' => Carbon\Carbon::now()->subDay()]);

				$newTrans = new Transactions();
				$newTrans->user_id = $finduser->id;
				$newTrans->amount = $amount;
				$newTrans->type = 2;
				$newTrans->status = 1;
				$newTrans->created_date = Carbon\Carbon::now();
				$newTrans->confirm_date = Carbon\Carbon::now();
				$newTrans->save();

				if($finduser != null) {
					if($finduser->lang == "tr") {

						$newNotify = new Notifications();
						$newNotify->user_id = $finduser->id;
						$newNotify->title = "Siparişiniz alındı!";
						$newNotify->notification = "Premium üyeliğiniz onaylanmıştır!";
						$newNotify->firebase = $finduser->firebase;
						$newNotify->status = 0;
						$newNotify->created_at = Carbon\Carbon::now();
						$newNotify->save();

					} else {

						$newNotify = new Notifications();
						$newNotify->user_id = $finduser->id;
						$newNotify->title = "Your order has been received!";
						$newNotify->notification = "Your subscription has been confirmed!";
						$newNotify->firebase = $finduser->firebase;
						$newNotify->status = 0;
						$newNotify->created_at = Carbon\Carbon::now();
						$newNotify->save();

					}
				}

			} else {

				$finduser = User::where('id', $request->user_id)->first();
				if($finduser != null) {
					if($finduser->language == "tr") {

						$newNotify = new Notifications();
						$newNotify->user_id = $finduser->id;
						$newNotify->title = "Geçersiz Ödeme!";
						$newNotify->notification = "Siparişiniz kontrol edilene kadar hesabınız kilitlenmiştir!";
						$newNotify->firebase = $finduser->firebase;
						$newNotify->status = 0;
						$newNotify->created_at = Carbon\Carbon::now();
						$newNotify->save();

					} else {

						$newNotify = new Notifications();
						$newNotify->user_id = $finduser->id;
						$newNotify->title = "Invalid Purchase!";
						$newNotify->notification = "Your account has been locked until your order has been checked!";
						$newNotify->firebase = $finduser->firebase;
						$newNotify->status = 0;
						$newNotify->created_at = Carbon\Carbon::now();
						$newNotify->save();

					}
				}
				$block = User::where('id', $request->user_id)->update(['blocked' => 1, 'blocked_reason' => 'Cheat!']);


			}
		}
        $response["success"] = true;
        echo json_encode($response);
            
        
    }

}