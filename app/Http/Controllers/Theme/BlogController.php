<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller {

    public function getBlog() {
        
        $menu = (new FunctionsController)->getMenu();
        
        return view('theme/blog', compact('menu'));

    }
    
    public function getBlogDetail($slug) {
        
        $menu = (new FunctionsController)->getMenu();
        
        $blog = Blog::where('slug', $slug)->first();
        
        if($blog) {
            
        } else {
            abort(404);
        }
        
        return view('theme/blog-detail', compact('menu'));

    }
    

}