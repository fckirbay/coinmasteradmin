<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Illuminate\Http\Request;
use Redirect;
use Sentinel;
use App\UserJokers;

class BankController extends Controller {

    public function getBank() {

    	$jokers = UserJokers::where('id', Sentinel::getUser()->id)->first();
                
        return view('cardgame/bank', compact('jokers'));

    }
    
}