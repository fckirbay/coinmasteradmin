<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Validator;
use Sentinel;
use Alert;
use Carbon;
use App\User;
use App\Notifications;
use App\UserReferences;

class RegisterController extends Controller {
    
    public function getRegister() {
        
        /*$json = json_decode(file_get_contents('https://www.cognalys.com/api/v1/otp/?app_id=72227ff82acd41ed824b880&access_token=5ebac5d9e1245175f827909f39573fe3fd90e89d&mobile=905555949170'), true);

        dd($json['status']);
        exit;*/
        
        return view('mobile/register', compact('menu'));

    }
    
    public function setRegister(Request $request) {

        if (strlen($request->password) < 5) {
            Alert::error(\Lang::get('general.password_length_must_be_min_5'));
            return Redirect::back();
        }

        if (strlen($request->password) > 20) {
            Alert::error(\Lang::get('general.password_length_must_be_max_20'));
            return Redirect::back();
        }

        if (strlen($request->username) < 4) {
            Alert::error(\Lang::get('general.username_length_must_be_min_4'));
            return Redirect::back();
        }

        if (strlen($request->password) > 30) {
            Alert::error("Password lenght must be max 30 chars!");
            return Redirect::back();
        }

        if (strlen($request->email) > 80) {
            Alert::error("Email lenght must be max 80 chars!");
            return Redirect::back();
        }

        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=+¬]/', $request->username)) {
            Alert::error("You can not use special chars in your username!");
            return Redirect::back();
        }
        
        if($request->password != null && $request->password_confirm != null) {
            if($request->password != $request->password_confirm) {
                Alert::error(\Lang::get('general.passwords_not_match'));
                return Redirect::back();
            }
        } else {
            Alert::error(\Lang::get('general.passwords_cant_left_blank'));
            return Redirect::back();
        }
        
        /*$control = User::where('tc_number', $request->country_code . $request->mobile_phone)->count();
        if($control > 0) {
            Alert::error(\Lang::get('general.this_phone_number_is_registered'));
            return Redirect::back();
        }*/
        
        $control = User::where('first_name', $request->username)->count();
        if($control > 0) {
            Alert::error(\Lang::get('general.this_username_is_registered'));
            return Redirect::back();
        }

        if(strlen($request->username) > 20) {
            Alert::error(\Lang::get('general.username_max_20'));
            return Redirect::back();
        }
        
        // Referans Üye Kontrolü
        if($request->reference_id != null) {
            $check_reference = User::where('id', $request->reference_id)->first();
            
            if($check_reference == null) {
                Alert::error(\Lang::get('general.invalid_reference_number'));
                return Redirect::back();
            } else {
                if($check_reference->verification == 0 || $check_reference->blocked == 1) {
                    Alert::error(\Lang::get('general.unverify_or_blocked_reference_number'));
                    return Redirect::back();
                }
            }

            
        }
        
        $credentials = [
            'email'    => strtolower($request->username) . '@email.com',
            'password' => $request->password,
            'first_name' => strtolower($request->username),
            //'birthday' => Carbon\Carbon::createFromFormat('d/m/Y', $request->birthday)->toDateTimeString(),
            'ticket' => 10,
            'email_2' => $request->email,
            'app_version' => config('app.application_version')
        ];
        
        
        $user = Sentinel::registerAndActivate($credentials);
            
        $role = Sentinel::findRoleByName('user');
            
        $user->roles()->attach($role);
            
        $credentials = [
            'email'    => $request->username . '@email.com',
            'password' => $request->password,
        ];
    
        $user = Sentinel::authenticateAndRemember($credentials);
            
        // Referans Üye Kontrolü
        if($request->reference_id != null) {

        $checkBefore = UserReferences::where('user_id', Sentinel::getUser()->id)->where('reference_id', $request->reference_id)->count();

       if($checkBefore == 0) {

            $newRef = new UserReferences();
            $newRef->user_id = Sentinel::getUser()->id;
            $newRef->reference_id = $request->reference_id;
            $newRef->date = Carbon\Carbon::now();
            $newRef->status = 0;
            $newRef->save();

            $add_reference = User::where('id', $user->id)->update(['reference_id' => $request->reference_id]);
       }
                
            /*
            $give_ticket = User::where('id', $request->reference_id)->increment('ticket', 1);
            $add_reference_count = User::where('id', $request->reference_id)->increment('reference_count', 1);
            $give_ref_ticket = User::where('id', $user->id)->increment('ticket', 1);
            */
                
        }

        // Send notifications to admins
        /*
        $admin_1 = User::where('id', 1)->select('firebase')->first();
        $admin_2 = User::where('id', 8)->select('firebase')->first();
        $newNotify = new Notifications();
        $newNotify->user_id = 1;
        $newNotify->title = "Yeni Üye Kaydı";
        $newNotify->notification = $request->username . " kullanıcı adıyla yeni bir üye geldi.";
        $newNotify->firebase = $admin_1->firebase;
        $newNotify->status = 0;
        $newNotify->created_at = Carbon\Carbon::now();
        $newNotify->save();
        
        $newNotify = new Notifications();
        $newNotify->user_id = 8;
        $newNotify->title = "Yeni Üye Kaydı";
        $newNotify->notification = $request->username . " kullanıcı adıyla yeni bir üye geldi.";
        $newNotify->firebase = $admin_2->firebase;
        $newNotify->status = 0;
        $newNotify->created_at = Carbon\Carbon::now();
        $newNotify->save();
        */
        
        Alert::success(\Lang::get('general.welcome_to_luckybox'));
        return redirect('index');
        
    }
    
    public function setVerify(Request $request) {
        
        
        if (substr($request->mobile_phone, 0, 1) == "0") {
            Alert::error("Telefon numarası 0 ile başlayamaz!");
            return Redirect::back();
        }
        
        //$checkPhone = User::where('phone', $request->country_code.$request->mobile_phone)->where('verification', 1)->count();
        $checkPhone = User::where('phone', $request->country_code.$request->mobile_phone)->where('id', '!=', Sentinel::getUser()->id)->count();

        if(Sentinel::getUser()->verification_tries > 2) {
            Alert::error(trans('general.maximum_try_count'));
            return Redirect::back();
        }
        
        if($checkPhone > 0) {
            Alert::error(\Lang::get('general.this_phone_number_is_registered'));
            return Redirect::back();
        } else {

            $increase_tries = User::where('id', Sentinel::getUser()->id)->increment('verification_tries');

/*
            $ch = curl_init();
            // Set query data here with the URL
            curl_setopt($ch, CURLOPT_URL, 'https://www.cognalys.com/api/v1/otp/?app_id=72227ff82acd41ed824b880&access_token=5ebac5d9e1245175f827909f39573fe3fd90e89d&mobile=' . $request->country_code . $request->mobile_phone); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            $content = trim(curl_exec($ch));
            curl_close($ch);
            $json = json_decode($content, true);
            */
            
            
            $headers = array(
            	'Accept:application/json',
                'Content-Type:application/json',
                'Authorization: 27730EFD-B43B-4D11-A36B-9DBFB276DD4F'
            );
            
            $post = [
                'number' => $request->country_code . $request->mobile_phone,
                'type' => 'reverse_cli',
                'platform'   => 'web'
            ];
            
            $post = json_encode($post);
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_URL,"https://api.checkmobi.com/v1/validation/request");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $server_output = curl_exec($ch);
            
            curl_close ($ch);
            
            
            $json = json_decode($server_output, true);
            
            
            

        
            if(!isset($json['error']) && isset($json['id'])) {


                $countries = [
                    '44' => 'UK (+44)',
                    '1' => 'USA (+1)',
                    '213' => 'Algeria (+213)',
                    '376' => 'Andorra (+376)',
                    '244' => 'Angola (+244)',
                    '1264' => 'Anguilla (+1264)',
                    '1268' => 'Antigua & Barbuda (+1268)',
                    '54' => 'Argentina (+54)',
                    '374' => 'Armenia (+374)',
                    '297' => 'Aruba (+297)',
                    '61' => 'Australia (+61)',
                    '43' => 'Austria (+43)',
                    '994' => 'Azerbaijan (+994)',
                    '1242' => 'Bahamas (+1242)',
                    '973' => 'Bahrain (+973)',
                    '880' => 'Bangladesh (+880)',
                    '1246' => 'Barbados (+1246)',
                    '375' => 'Belarus (+375)',
                    '32' => 'Belgium (+32)',
                    '501' => 'Belize (+501)',
                    '229' => 'Benin (+229)',
                    '1441' => 'Bermuda (+1441)',
                    '975' => 'Bhutan (+975)',
                    '591' => 'Bolivia (+591)',
                    '387' => 'Bosnia Herzegovina (+387)',
                    '267' => 'Botswana (+267)',
                    '55' => 'Brazil (+55)',
                    '673' => 'Brunei (+673)',
                    '359' => 'Bulgaria (+359)',
                    '226' => 'Burkina Faso (+226)',
                    '257' => 'Burundi (+257)',
                    '855' => 'Cambodia (+855)',
                    '237' => 'Cameroon (+237)',
                    //'1' => 'Canada (+1)',
                    '238' => 'Cape Verde Islands (+238)',
                    '1345' => 'Cayman Islands (+1345)',
                    '236' => 'Central African Republic (+236)',
                    '56' => 'Chile (+56)',
                    '86' => 'China (+86)',
                    '57' => 'Colombia (+57)',
                    '269' => 'Comoros (+269)',
                    '242' => 'Congo (+242)',
                    '682' => 'Cook Islands (+682)',
                    '506' => 'Costa Rica (+506)',
                    '385' => 'Croatia (+385)',
                    '53' => 'Cuba (+53)',
                    '90392' => 'Cyprus North (+90392)',
                    '357' => 'Cyprus South (+357)',
                    '42' => 'Czech Republic (+42)',
                    '45' => 'Denmark (+45)',
                    '253' => 'Djibouti (+253)',
                    '1809' => 'Dominica (+1809)',
                    '1809' => 'Dominican Republic (+1809)',
                    '593' => 'Ecuador (+593)',
                    '20' => 'Egypt (+20)',
                    '503' => 'El Salvador (+503)',
                    '240' => 'Equatorial Guinea (+240)',
                    '291' => 'Eritrea (+291)',
                    '372' => 'Estonia (+372)',
                    '251' => 'Ethiopia (+251)',
                    '500' => 'Falkland Islands (+500)',
                    '298' => 'Faroe Islands (+298)',
                    '679' => 'Fiji (+679)',
                    '358' => 'Finland (+358)',
                    '33' => 'France (+33)',
                    '594' => 'French Guiana (+594)',
                    '689' => 'French Polynesia (+689)',
                    '241' => 'Gabon (+241)',
                    '220' => 'Gambia (+220)',
                    '7880' => 'Georgia (+7880)',
                    '49' => 'Germany (+49)',
                    '233' => 'Ghana (+233)',
                    '350' => 'Gibraltar (+350)',
                    '30' => 'Greece (+30)',
                    '299' => 'Greenland (+299)',
                    '1473' => 'Grenada (+1473)',
                    '590' => 'Guadeloupe (+590)',
                    '671' => 'Guam (+671)',
                    '502' => 'Guatemala (+502)',
                    '224' => 'Guinea (+224)',
                    '245' => 'Guinea - Bissau (+245)',
                    '592' => 'Guyana (+592)',
                    '509' => 'Haiti (+509)',
                    '504' => 'Honduras (+504)',
                    '852' => 'Hong Kong (+852)',
                    '36' => 'Hungary (+36)',
                    '354' => 'Iceland (+354)',
                    '91' => 'India (+91)',
                    '62' => 'Indonesia (+62)',
                    '98' => 'Iran (+98)',
                    '964' => 'Iraq (+964)',
                    '353' => 'Ireland (+353)',
                    '972' => 'Israel (+972)',
                    '39' => 'Italy (+39)',
                    '1876' => 'Jamaica (+1876)',
                    '81' => 'Japan (+81)',
                    '962' => 'Jordan (+962)',
                    '7' => 'Kazakhstan (+7)',
                    '254' => 'Kenya (+254)',
                    '686' => 'Kiribati (+686)',
                    '850' => 'Korea North (+850)',
                    '82' => 'Korea South (+82)',
                    '965' => 'Kuwait (+965)',
                    '996' => 'Kyrgyzstan (+996)',
                    '856' => 'Laos (+856)',
                    '371' => 'Latvia (+371)',
                    '961' => 'Lebanon (+961)',
                    '266' => 'Lesotho (+266)',
                    '231' => 'Liberia (+231)',
                    '218' => 'Libya (+218)',
                    '417' => 'Liechtenstein (+417)',
                    '370' => 'Lithuania (+370)',
                    '352' => 'Luxembourg (+352)',
                    '853' => 'Macao (+853)',
                    '389' => 'Macedonia (+389)',
                    '261' => 'Madagascar (+261)',
                    '265' => 'Malawi (+265)',
                    '60' => 'Malaysia (+60)',
                    '960' => 'Maldives (+960)',
                    '223' => 'Mali (+223)',
                    '356' => 'Malta (+356)',
                    '692' => 'Marshall Islands (+692)',
                    '596' => 'Martinique (+596)',
                    '222' => 'Mauritania (+222)',
                    '269' => 'Mayotte (+269)',
                    '52' => 'Mexico (+52)',
                    '691' => 'Micronesia (+691)',
                    '373' => 'Moldova (+373)',
                    '377' => 'Monaco (+377)',
                    '976' => 'Mongolia (+976)',
                    '1664' => 'Montserrat (+1664)',
                    '212' => 'Morocco (+212)',
                    '258' => 'Mozambique (+258)',
                    '95' => 'Myanmar (+95)',
                    '264' => 'Namibia (+264)',
                    '674' => 'Nauru (+674)',
                    '977' => 'Nepal (+977)',
                    '31' => 'Netherlands (+31)',
                    '687' => 'New Caledonia (+687)',
                    '64' => 'New Zealand (+64)',
                    '505' => 'Nicaragua (+505)',
                    '227' => 'Niger (+227)',
                    '234' => 'Nigeria (+234)',
                    '683' => 'Niue (+683)',
                    '672' => 'Norfolk Islands (+672)',
                    '670' => 'Northern Marianas (+670)',
                    '47' => 'Norway (+47)',
                    '968' => 'Oman (+968)',
                    '680' => 'Palau (+680)',
                    '507' => 'Panama (+507)',
                    '675' => 'Papua New Guinea (+675)',
                    '595' => 'Paraguay (+595)',
                    '51' => 'Peru (+51)',
                    '63' => 'Philippines (+63)',
                    '48' => 'Poland (+48)',
                    '351' => 'Portugal (+351)',
                    '1787' => 'Puerto Rico (+1787)',
                    '974' => 'Qatar (+974)',
                    '262' => 'Reunion (+262)',
                    '40' => 'Romania (+40)',
                    '7' => 'Russia (+7)',
                    '250' => 'Rwanda (+250)',
                    '378' => 'San Marino (+378)',
                    '239' => 'Sao Tome & Principe (+239)',
                    '966' => 'Saudi Arabia (+966)',
                    '221' => 'Senegal (+221)',
                    '381' => 'Serbia (+381)',
                    '248' => 'Seychelles (+248)',
                    '232' => 'Sierra Leone (+232)',
                    '65' => 'Singapore (+65)',
                    '421' => 'Slovak Republic (+421)',
                    '386' => 'Slovenia (+386)',
                    '677' => 'Solomon Islands (+677)',
                    '252' => 'Somalia (+252)',
                    '27' => 'South Africa (+27)',
                    '34' => 'Spain (+34)',
                    '94' => 'Sri Lanka (+94)',
                    '290' => 'St. Helena (+290)',
                    '1869' => 'St. Kitts (+1869)',
                    '1758' => 'St. Lucia (+1758)',
                    '249' => 'Sudan (+249)',
                    '597' => 'Suriname (+597)',
                    '268' => 'Swaziland (+268)',
                    '46' => 'Sweden (+46)',
                    '41' => 'Switzerland (+41)',
                    '963' => 'Syria (+963)',
                    '886' => 'Taiwan (+886)',
                    '7' => 'Tajikstan (+7)',
                    '66' => 'Thailand (+66)',
                    '228' => 'Togo (+228)',
                    '676' => 'Tonga (+676)',
                    '1868' => 'Trinidad & Tobago (+1868)',
                    '216' => 'Tunisia (+216)',
                    '90' => 'Turkey (+90)',
                    '7' => 'Turkmenistan (+7)',
                    '993' => 'Turkmenistan (+993)',
                    '1649' => 'Turks & Caicos Islands (+1649)',
                    '688' => 'Tuvalu (+688)',
                    '256' => 'Uganda (+256)',
                    '380' => 'Ukraine (+380)',
                    '971' => 'United Arab Emirates (+971)',
                    '598' => 'Uruguay (+598)',
                    '7' => 'Uzbekistan (+7)',
                    '678' => 'Vanuatu (+678)',
                    '379' => 'Vatican City (+379)',
                    '58' => 'Venezuela (+58)',
                    '84' => 'Vietnam (+84)',
                    '84' => 'Virgin Islands - British (+1284)',
                    '84' => 'Virgin Islands - US (+1340)',
                    '681' => 'Wallis & Futuna (+681)',
                    '969' => 'Yemen (North)(+969)',
                    '967' => 'Yemen (South)(+967)',
                    '260' => 'Zambia (+260)',
                    '263' => 'Zimbabwe (+263)',
                ];

                if(isset($countries[$request->country_code])) {
                    $country = $countries[$request->country_code];
                } else {
                    $country = "Unknown";
                }
                
                $updatePhone = User::where('id', Sentinel::getUser()->id)->update(['phone' => $request->country_code . $request->mobile_phone, 'country' => $country, 'verify_id' => $json['id']]);
                
                session(['verify_id' => $json['id']]);


                
                return view('mobile/verify-complete', compact('json'));
                
            } else {
                Alert::error(\Lang::get('general.verification_failed'));
                return Redirect::back();
            }
        
        }
        
        
    }
    
    public function setVerifyComplete(Request $request) {
        /*
        {
          "status": "success",
          "message": "SUCCESS_MESSAGE"
        }
        
        {
          "status": "failed",
          "mobile": "REQUESTED_MOBILE_NUMBER",
          "errors": {
            "ERROR_CODE": "ERROR_MESSAGE"
          }
        }
        */
        
        /*if(Sentinel::getUser()->phone != $request->mobile) {
            Alert::error(\Lang::get('general.verification_failed'));
            return Redirect::back();
        }*/
        
        /*
        $ch = curl_init();
            // Set query data here with the URL
            curl_setopt($ch, CURLOPT_URL, 'https://www.cognalys.com/api/v1/otp/confirm/?access_token=5ebac5d9e1245175f827909f39573fe3fd90e89d&app_id=72227ff82acd41ed824b880&otp='.$request->otp_start.$request->otp_code.'&keymatch='.$request->keymatch); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            $content = trim(curl_exec($ch));
            curl_close($ch);
            */
            
            $headers = array(
            	'Accept:application/json',
                'Content-Type:application/json',
                'Authorization: 27730EFD-B43B-4D11-A36B-9DBFB276DD4F'
            );
            
            $post = [
                'id' => $request->verify_id,
                'pin' => $request->otp_code
            ];
            
            $post = json_encode($post);
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_URL,"https://api.checkmobi.com/v1/validation/verify");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $server_output = curl_exec($ch);
            
            curl_close ($ch);
            
            
            
            $json = json_decode($server_output, true);
            
            
            

        if($json['validated'] == true || $json['validated'] == "true" || $json['validated'] == 1 || $json['validated'] == "1") {
            
            if(substr(Sentinel::getUser()->phone, 0, 2) == "90") {
                $language = "tr";
                $currency = "try";
            } else {
                $language = "en";
                $currency = "eur";
            }

            $userIp = $_SERVER['REMOTE_ADDR'];
            
            $update = User::where('id', Sentinel::getUser()->id)->update(['lang' => $language, 'currency' => $currency, 'verification' => 1, 'ip_address' => $userIp]);

            if(Sentinel::getUser()->reference_id != null) {
                
                $findRef = User::where('id', Sentinel::getUser()->reference_id)->first();
                
                if((Sentinel::getUser()->country == "USA (+1)" || Sentinel::getUser()->country == "Canada (+1)") && $findRef->country == "Turkey (+90)") {
                    $block = User::where('id', Sentinel::getUser()->id)->update(['blocked' => 1]);
                    Alert::error(\Lang::get('general.invalid_reference_number'));
                    return redirect('index');
                }

                $checkBefore = UserReferences::where('user_id', Sentinel::getUser()->id)->where('reference_id', Sentinel::getUser()->reference_id)->count();

                $total_references = UserReferences::where('reference_id', Sentinel::getUser()->reference_id)->count();

                if($checkBefore > 0) {

                if($total_references < 25) {
                    $give_ticket = User::where('id', Sentinel::getUser()->reference_id)->increment('ticket', 1);
                    $add_reference_count = User::where('id', Sentinel::getUser()->reference_id)->increment('reference_count', 1);
                } elseif($total_references < 50) {
                    $give_ticket = User::where('id', Sentinel::getUser()->reference_id)->increment('ticket', 0.50);
                    $add_reference_count = User::where('id', Sentinel::getUser()->reference_id)->increment('reference_count', 1);
                } else {
                    $give_ticket = User::where('id', Sentinel::getUser()->reference_id)->increment('ticket', 0.25);
                    $add_reference_count = User::where('id', Sentinel::getUser()->reference_id)->increment('reference_count', 1);
                }
                    
                    if(Sentinel::getUser()->ticket == 10){
                        $give_ref_ticket = User::where('id', Sentinel::getUser()->id)->increment('ticket', 1);
                    }
                    
                    $confirmRef = UserReferences::where('user_id', Sentinel::getUser()->id)->where('reference_id', Sentinel::getUser()->reference_id)->update(['status' => 1]);
                }
                
            }
            
            
            Alert::success(\Lang::get('general.your_registration_has_been_success'));
            return redirect('index');
            
        } else {
            
            Alert::error(\Lang::get('general.verification_failed'));
            return Redirect::back();
        }
        
    }
    
    public function getVerify() {
        
        return view('mobile/verify');

    }
    
    function split_name($name, $type) {
        $name_surname = $name;
        
        $explode = explode(" ", $name_surname);
        
        if($type == 1) {
            $last_name = end($explode);
            $new_array = array_slice($explode,0,-1);
            $first_name = implode(' ',$new_array);
        } elseif($type == 2) {
            $first_name = $explode[0];
            $last_name = $explode[1] . ' ' . $explode[2];
        } else {
            $first_name = $explode[0] . ' ' . $explode[1];
            $last_name = $explode[2] . ' ' . $explode[3];
        }
        
                
        return array($first_name, $last_name);
    }
    

}