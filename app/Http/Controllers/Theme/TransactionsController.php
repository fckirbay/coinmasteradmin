<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use Validator;
use App\User;
use App\Transactions;

class TransactionsController extends Controller {

    public function getTransactions() {
        
        $transactions = Transactions::where('user_id', Sentinel::getUser()->id)->orderBy('id', 'desc')->get();
        
        return view('mobile/transactions', compact('transactions'));

    }
    
}