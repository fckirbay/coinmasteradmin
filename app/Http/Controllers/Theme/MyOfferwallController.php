<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use Validator;
use App\User;
use App\Offerwall;
use DB;

class MyOfferwallController extends Controller {

    public function getMyOfferwall() {
        
        $tasks = Offerwall::where('user_id', Sentinel::getUser()->id)->where('is_completed', 1)->orderBy('id', 'desc')->get();
        
        return view('mobile/my-offerwall', compact('tasks'));

    }
    
}