<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use Validator;
use App\User;
use App\Clicks;
use DB;

class OpenBoxesController extends Controller {

    public function getOpenBoxes() {
        
        $openBoxes = DB::table('clicks')
            ->leftjoin('rooms', 'clicks.city', '=', 'rooms.id')
            ->select('clicks.*', 'rooms.name', 'rooms.name_en')
            ->where('clicks.user_id', Sentinel::getUser()->id)
            ->orderBy('id', 'desc')
            ->get();
        
        return view('mobile/open-boxes', compact('openBoxes'));

    }
    
}