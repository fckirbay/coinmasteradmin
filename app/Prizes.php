<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prizes extends Model
{
    protected $table = 'prizes';
    public $timestamps = false;
}
