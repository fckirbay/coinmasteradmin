<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotes extends Model
{
    protected $table = 'user_notes';
    public $timestamps = false;
}
