<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scratches extends Model
{
    protected $table = 'scratches';
    public $timestamps = false;
}
