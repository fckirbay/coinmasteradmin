<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxesNew extends Model
{
    protected $table = 'boxes_new';
    public $timestamps = false;
}
