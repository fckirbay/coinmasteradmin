				<script type="text/javascript" src="{{ asset('assets/cardgame/scripts/jquery.js') }}"></script>
				<script type="text/javascript" src="{{ asset('assets/cardgame/scripts/plugins.js') }}"></script>
				<script type="text/javascript" src="{{ asset('assets/cardgame/scripts/custom.js') }}"></script>
				<!-- Sweetalert -->
				<script src="{{ asset('assets/admin/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
				@include('sweet::alert')