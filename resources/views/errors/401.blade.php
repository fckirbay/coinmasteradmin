@extends('layouts.cardgame.main')
@section('styles')

@endsection
@section('content')
         <div class="page-content-black"></div>
         <div class="page-content">
            <div class="cover-wrapper cover-no-buttons">
               <div data-height="cover" class="caption bottom-0">
                  <div class="caption-center">
                     <h1 class="center-text"><i class="fa fa-exclamation-triangle color-red2-dark fa-3x bottom-20"></i></h1>
                     <h1 class="color-white center-text fa-5x uppercase bolder bottom-10 top-20">ERROR</h1>
                     <h2 class="color-white center-text uppercase bolder bottom-30 font-15">401 - Your IP blocked. Woops!</h2>
                     <p class="boxed-text-large">
                        Üzgünüz, IP adresiniz sunucularımızda çok da sevilmiyor. Detaylı bilgi almak istiyorsanız info@cardgame.com adresinden bize ulaşabilirsiniz.
                     </p>
                  </div>
                  <div class="caption-overlay bg-black opacity-80"></div>
                  <div class="caption-bg" style="background-image:url(assets/cardgame/images/pictures/18t.jpg)"></div>
               </div>
            </div>
         </div>
@endsection
@section('scripts')

@endsection