@extends('layouts.cardgame.main')
@section('styles')

@endsection
@section('content')
			<div class="page-content header-clear-medium" style="padding-top: 50px; padding-bottom: 0px">
				<div class="content-boxed" style="margin: 0px 0px 0px; border-radius: 0px !important">
					<div class="content bottom-5">
						<div class="link-list link-list-2 link-list-long-border">
							@forelse($references as $key => $val)
							<a href="#" class="no-border">
								@if($val->verification == 0)
									<i class="fa fa-times-circle color-red2-dark"></i>
								@else
									<i class="fa fa-check-circle color-green2-dark"></i>
								@endif
								<span>{{ $val->first_name }}</span>
								<strong>{{ Carbon\Carbon::parse($val->created_at)->format('d M H:i') }}</strong>
								<i class="fa fa-angle-right"></i>
							</a>
							@empty
							Kayıt yok.
							@endforelse
						</div>
					</div>
				</div>
			</div>
@endsection
@section('scripts')
  
@endsection