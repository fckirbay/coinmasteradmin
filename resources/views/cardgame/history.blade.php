@extends('layouts.cardgame.main')
@section('styles')

@endsection
@section('content')
			<div class="page-content header-clear-medium" style="padding-top: 50px; padding-bottom: 0px">
				<div class="content-boxed" style="margin: 0px 0px 0px; border-radius: 0px !important">
					<div class="content bottom-5">
						<div class="link-list link-list-2 link-list-long-border">
							<a href="#" class="no-border">
								<i class="fa fa-times color-red2-dark"></i>
								<span>@lang('general.lost') (8 @lang('general.lap'))</span>
								<em class="bg-red2-dark">0 Coin</em>
								<strong>05.04.2019 18:34:43</strong>
								<i class="fa fa-angle-right"></i>
							</a>
							<a href="#" class="no-border">
								<i class="fa fa-check color-green2-dark"></i>
								<span>@lang('general.won') (4 @lang('general.lap'))</span>
								<em class="bg-green2-dark">8 @lang('general.coin')</em>
								<strong>05.04.2019 18:34:43</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
@endsection
@section('scripts')
  
@endsection