@extends('layouts.cardgame.main')
@section('styles')
@endsection
@section('content')
<div class="page-content header-clear-medium content-boxed" style="margin: 0px 0px 0px; border-radius: 0px">
	<div class="content bottom-0" style="height:100%; overflow:hidden">
		@forelse($messages->reverse() as $key => $val)
            @if($val->owner == 1)
	            <div class="speech-bubble speech-right color-black">
					{{ $val->message }}
					<em class="speech-read bottom-20" style="margin-bottom: 0px !important;">{{ Carbon\Carbon::parse($val->date)->format('d M H:i') }}</em>
				</div>
				<div class="clear"></div>
	        @else
		        <div class="speech-bubble speech-left bg-highlight">
					{{ $val->message }}
					<em class="speech-read bottom-20" style="margin-bottom: 0px !important;">{{ Carbon\Carbon::parse($val->date)->format('d M H:i') }}</em>
				</div>
				<div class="clear"></div>
	        @endif
	    @empty
	    @endforelse
	</div>
	<div class="content" style="bottom:0;position:fixed; width:92%">
		<div class="input-style input-style-2 input-required">
			<span>Enter your Message</span>
			<em>(required)</em>
			<textarea placeholder=""></textarea>
		</div>
	</div>
</div>
@endsection
@section('scripts')
@endsection