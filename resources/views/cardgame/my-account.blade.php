@extends('layouts.cardgame.main')
@section('styles')
@endsection
@section('content')
<div class="page-content header-clear-medium" style="padding-top: 50px;">
	<div class="content-boxed" style="margin: 0 0 0 !important; border-radius: 0px !important;">
		<div class="content">
			<h3 class="bolder" style="text-align: center;">@lang('general.my_account')</h3>
			<p style="text-align: center">
				@lang('general.you_can_edit_your_account_information')
			</p>
			{!! Form::open(['url'=>'my-account', 'method'=>'post', 'class'=>'register-form outer-top-xs'])  !!}
				<div class="input-style input-style-2 input-required">
					<em><i class="fa fa-angle-down"></i></em>
					<select name="lang">
						<option value="en" @if(Sentinel::getUser()->lang === "en") selected @endif>@lang('general.english')</option>
	                    <option value="de" @if(Sentinel::getUser()->lang === "de") selected @endif>@lang('general.german')</option>
	                    <option value="tr" @if(Sentinel::getUser()->lang === "tr") selected @endif>@lang('general.turkish')</option>
					</select>
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fa fa-at"></i>
					<input type="email" name="email" placeholder="@lang('general.email')" value="{{ Sentinel::getUser()->email_2 }}" disabled>
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fa fa-user"></i>
					<input type="name" name="username" placeholder="@lang('general.username')" value="{{ Sentinel::getUser()->first_name }}" disabled>
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fas fa-phone"></i>
					<input type="name" name="phone" placeholder="@lang('general.phone')" value="{{ Sentinel::getUser()->phone }}" disabled>
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fas fa-asterisk"></i>
					<input type="password" name="password" placeholder="@lang('general.password')">
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fas fa-asterisk"></i>
					<input type="password" name="password_confirm" placeholder="@lang('general.password') (@lang('general.again'))">
				</div>
				<button type="submit" class="back-button button button-full button-m shadow-large button-round-small bg-highlight top-30 bottom-0" style="width:100%">@lang('general.save')</button>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endsection
@section('scripts')
@endsection