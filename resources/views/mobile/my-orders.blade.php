@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
            @forelse($orders as $key => $val)
            <div href="#" class="invoice-box">
               <a data-toggle-box="invoice-{{ $key }}" href="#" class="invoice-box-header">
               <em>#{{ $val->id }}</em>
               <strong>{{ Carbon\Carbon::parse($val->created_at)->format('d M Y') }}</strong>
               <span>{{ $val->name_surname }}</span>
               <u class="color-black">{{ number_format($val->total_amount, 2, ',', '.') }}₺ <del class="color-gray-dark">@if($val->order_status == 0)Onay Bekliyor
                    					  @elseif($val->order_status == 1)Hazırlanıyor
                    					  @elseif($val->order_status == 2)Kargolandı
                    					  @elseif($val->order_status == 3)Tamamlandı
                    					  @elseif($val->order_status == 4)İptal Edildi
                    					  @elseif($val->order_status == 5)İade Edildi
                    					  @elseif($val->order_status == 6)Reddedildi
                    					  @endif</del></u>
               </a>
               <div id="invoice-{{ $key }}" class="invoice-box-content">
                  <div class="decoration"></div>
                  <div class="invoice-box-item">
                     <em>İsim Soyisim:</em>
                     <strong>{{ $val->name_surname }}</strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Toplam Tutar:</em>
                     <strong>{{ number_format($val->total_amount, 2, ',', '.') }}₺</strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Kargo Ücreti:</em>
                     <strong>@if($val->shipping_price > 0){{ number_format($val->shipping_price, 2, ',', '.') }}₺@else Ücretsiz @endif</strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Ödeme Durumu</em>
                     <strong>@if($val->order_status == 0)Onay Bekliyor
                    			  @else Onaylandı
                    			  @endif</strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Ödeme Türü </em>
                     <strong>@if($val->payment_method == 1) Kredi Kartı @elseif($val->payment_method == 2) Debit Kart @elseif($val->payment_method == 3) Havale/EFT @endif</strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Kargo Durumu</em>
                     <strong>@if($val->shipping == null) Kargo Bekleniyor
                     @else Takip Kodu: {{ $val->shipping }} @endif</strong>
                  </div>
                  <div class="invoice-box-item bottom-20">
                     <em>Ürünler</em>
                     @forelse($products as $key => $val)
                     <a href="{{ url($products[$key]['slug']) }}">{{ $products[$key]['title'] }} @if($products[$key]['qty'] > 1)x{{ $products[$key]['qty'] }}@endif<u>{{ number_format($products[$key]['total_price'], 2, ',', '.') }}₺</u></a>
                     @empty
                     
                     @endforelse
                  </div>
                  <div class="decoration"></div>
                  <a href="#" class="button bg-highlight button-rounded button-full button-s uppercase ultrabold bottom-30">Faturayı İndir</a>
               </div>
            </div>
            @empty
            <div style="text-align:center; margin-top:20px">Hiç siparişiniz yok.</div>
            @endforelse
            
@endsection
@section('scripts')
   
@endsection