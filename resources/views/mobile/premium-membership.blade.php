@extends('layouts.mobile.main')
@section('styles')

@endsection
@section('content')



            <div class="decoration bottom-0"></div>
            @if($totalTrans - $withdraws <= 30 && Sentinel::getUser()->country != "Turkey (+90)")
               <div class="store-slide-2">
                  <a href="#" class="store-slide-image">
                  <img class="preload-image" src="{{ asset('assets/mobile/images/memberships/bronze.png') }}" data-src="{{ asset('assets/mobile/images/memberships/bronze.png') }}" alt="img">
                  </a>
                  <div class="store-slide-title" id="bronze">
                     <strong>Bronze @lang('general.membership')</strong>
                     <em class="color-gray-dark">@lang('general.click_for_membership_benefits')</em>
                  </div>
                  @if(Sentinel::getUser()->premium_membership == null || Sentinel::getUser()->premium_expiration < Carbon\Carbon::now())
                     
                        <div class="store-slide-button">
                           @if(Sentinel::check() && Sentinel::getUser()->currency == "try")
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> 5,00 ₺</strong>
                              <button class="uppercase ultrabold button-xxs button button-blue" onClick="buyPremium('bronze_tr', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                          @else
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> €5.00</strong>
                              <button class="uppercase ultrabold button-xxs button button-blıe" onClick="buyPremium('bronze_en', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                           @endif
                        </div>
                  @endif
               </div>
            @endif
            @if($totalTrans - $withdraws <= 30)
               <div class="decoration bottom-0"></div>
               <div class="store-slide-2">
                  <a href="#" class="store-slide-image">
                  <img class="preload-image" src="{{ asset('assets/mobile/images/memberships/silver/premium.png') }}" data-src="{{ asset('assets/mobile/images/memberships/silver.png') }}" alt="img">
                  </a>
                  <div class="store-slide-title" id="silver">
                     <strong>Silver @lang('general.membership')</strong>
                     <em class="color-gray-dark">@lang('general.click_for_membership_benefits')</em>
                  </div>
                  @if(Sentinel::getUser()->premium_membership == null || Sentinel::getUser()->premium_expiration < Carbon\Carbon::now())
                        <div class="store-slide-button">
                           @if(Sentinel::check() && Sentinel::getUser()->currency == "try")
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> 10,00 ₺</strong>
                              <button class="uppercase ultrabold button-xxs button button-blue" onClick="buyPremium('silver_tr', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                          @else
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> €10.00</strong>
                              <button class="uppercase ultrabold button-xxs button button-blıe" onClick="buyPremium('silver_en', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                           @endif
                        </div>
                  @endif
               </div>
            @endif
            @if($totalTrans - $withdraws <= 20)
               <div class="decoration bottom-0"></div>
               <div class="store-slide-2">
                  <a href="#" class="store-slide-image">
                  <img class="preload-image" src="{{ asset('assets/mobile/images/memberships/gold/premium.png') }}" data-src="{{ asset('assets/mobile/images/memberships/gold.png') }}" alt="img">
                  </a>
                  <div class="store-slide-title" id="gold">
                     <strong>Gold @lang('general.membership')</strong>
                     <em class="color-gray-dark">@lang('general.click_for_membership_benefits')</em>
                  </div>
                  @if(Sentinel::getUser()->premium_membership == null || Sentinel::getUser()->premium_expiration < Carbon\Carbon::now())
                        <div class="store-slide-button">
                           @if(Sentinel::check() && Sentinel::getUser()->currency == "try")
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> 20,00 ₺</strong>
                              <button class="uppercase ultrabold button-xxs button button-blue" onClick="buyPremium('gold_tr', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                          @else
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> €20.00</strong>
                              <button class="uppercase ultrabold button-xxs button button-blıe" onClick="buyPremium('gold_en', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                           @endif
                        </div>
                  @endif
               </div>
            @endif
            @if($totalTrans - $withdraws <= 15)
               <div class="decoration bottom-0"></div>
               <div class="store-slide-2">
                  <a href="#" class="store-slide-image">
                  <img class="preload-image" src="{{ asset('assets/mobile/images/memberships/platinum.png') }}" data-src="{{ asset('assets/mobile/images/memberships/platinum.png') }}" alt="img">
                  </a>
                  <div class="store-slide-title">
                     <strong>Platinum @lang('general.membership')</strong>
                     <em class="color-gray-dark">@lang('general.click_for_membership_benefits')</em>
                  </div>
                  @if(Sentinel::getUser()->premium_membership == null || Sentinel::getUser()->premium_expiration < Carbon\Carbon::now())
                        <div class="store-slide-button" id="platinum">
                           @if(Sentinel::check() && Sentinel::getUser()->currency == "try")
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> 30,00 ₺</strong>
                              <button class="uppercase ultrabold button-xxs button button-blue" onClick="buyPremium('platinum_tr', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                          @else
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> €30.00</strong>
                              <button class="uppercase ultrabold button-xxs button button-blıe" onClick="buyPremium('platinum_en', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                           @endif
                        </div>
                  @endif
               </div>
            @endif
            @if($totalTrans - $withdraws <= 0 && Sentinel::getUser()->currency != "try")
               <div class="decoration bottom-0"></div>
               <div class="store-slide-2">
                  <a href="#" class="store-slide-image">
                  <img class="preload-image" src="{{ asset('assets/mobile/images/memberships/vip.png') }}" data-src="{{ asset('assets/mobile/images/memberships/vip.png') }}" alt="img">
                  </a>
                  <div class="store-slide-title">
                     <strong>VIP @lang('general.membership')</strong>
                     <em class="color-gray-dark">@lang('general.click_for_membership_benefits')</em>
                  </div>
                  @if(Sentinel::getUser()->premium_membership == null || Sentinel::getUser()->premium_expiration < Carbon\Carbon::now())
                        <div class="store-slide-button" id="platinum">
                           @if(Sentinel::check() && Sentinel::getUser()->currency == "try")
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> 50,00 ₺</strong>
                              <button class="uppercase ultrabold button-xxs button button-blue" onClick="buyPremium('vip_tr', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                          @else
                              <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> €50.00</strong>
                              <button class="uppercase ultrabold button-xxs button button-blıe" onClick="buyPremium('vip_en2', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                           @endif
                        </div>
                  @endif
               </div>
            @endif
            <!--
            <div class="decoration bottom-0"></div>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/memberships/vip.png') }}" data-src="{{ asset('assets/mobile/images/memberships/vip.png') }}" alt="img">
               </a>
               <div class="store-slide-title" id="vip">
                  <strong>VIP @lang('general.membership')</strong>
                  <em class="color-gray-dark">@lang('general.click_for_membership_benefits')</em>
               </div>
               <div class="store-slide-button">
                  @if(Sentinel::check() && Sentinel::getUser()->currency == "try")
                     <strong> 100,00 ₺</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue" onClick="buyPremium('vip_tr', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                 @else
                     <strong> €100.00</strong>
                     <button class="uppercase ultrabold button-xxs button button-blıe" onClick="buyPremium('vip_en', '{{ Sentinel::getUser()->id }}');" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Buy Now</button>
                  @endif
               </div>
            </div>
         -->
@endsection
@section('scripts')
   <script>
      $( "#bronze" ).click(function() {
        window.location = "http://app.luckybox.fun/premium-membership-advantages/bronze";
      });
      $( "#silver" ).click(function() {
        window.location = "http://app.luckybox.fun/premium-membership-advantages/silver";
      });
      $( "#gold" ).click(function() {
        window.location = "http://app.luckybox.fun/premium-membership-advantages/gold";
      });
      $( "#platinum" ).click(function() {
        window.location = "http://app.luckybox.fun/premium-membership-advantages/platinum";
      });
      $( "#vip" ).click(function() {
        window.location = "http://app.luckybox.fun/premium-membership-advantages/vip";
      });       
   </script>
@endsection