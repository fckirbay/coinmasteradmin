@extends('layouts.mobile.main')
@section('styles')

@endsection
@section('content')
            <div class="page-content header-clear-larger">
            <div id="page-404">
               <h1 class="center-text bold">-</h1>
               <h4 class="center-text uppercase ultrabold bottom-30">@lang('general.a_newer_version_is_available')</h4>
               <p class="center-text bottom-30">
                  @lang('general.there_is_a_newer_version')
               </p>
               <a href="{{ url('/') }}" class="button bg-highlight button-s button-center button-rounded uppercase ultrabold">Home</a>
            </div>
         </div>
@endsection
@section('scripts')

@endsection