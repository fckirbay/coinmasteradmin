@extends('layouts.mobile.main')
@section('styles')
    <style>
        .inlineinput div {
    display: inline;
}
    </style>
@endsection
@section('content')
<div class="notification-large notification-has-icon notification-red notification-green bottom-0" style="text-align:center">
<div class="notification-icon"><i class="fa fa-exclamation-triangle notification-icon"></i></div>
<h1 class="uppercase ultrabold" style="font-size:14px">@lang('general.dont_open_missed_call')</h1>
<p>@lang('general.it_is_enough_to_enter_the_last_5')</p>
<a href="#" class="close-notification"><i class="fa fa-times"></i></a>
</div>

            <div class="page-login bottom-20">
                <h3 class="uppercase ultrabold top-30 bottom-0 center-text">@lang('general.verify_your_account')</h3>
                <!--<p class="smaller-text bottom-30" style="margin-bottom:10px !important">Ücretsiz 10 kutu açmak için şimdi kaydolun!</p>-->
                {!! Form::open(['url'=>'verify-complete', 'method'=>'post', 'class'=>'register-form outer-top-xs', 'id'=>'complete-verify'])  !!}
                <a class="forgot center-text">@lang('general.write_last_5_chars_of_missed_call')</a>
                    <input type="hidden" name="verify_id" value="{{ session('verify_id') }}"/>
                    <div class="page-login-field top-15">
                        <i class="fas fa-asterisk"></i>
                        <input type="number" name="otp_code" class="phone" placeholder="@lang('general.last_5_digits')" maxlength="4" required>
                        <em>(required)</em>
                    </div>
                    <button type="submit" class="button bg-highlight button-full button-rounded button-s uppercase ultrabold" id="complete">@lang('general.verify')</button>
                {!! Form::close() !!}
            </div>
            
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/mobile/scripts/jquery.mask.js') }}"></script>
    <script>
       $(document).ready(function(){
            $('.phone').mask('000000000000');
       });
       $('#complete-verify').submit(function(){
          $('#complete').hide();
       });
    </script>
@endsection