@extends('layouts.mobile.main')
@section('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        #overlay {
            background:rgba(0,0,0,0.3);
            display:none;
            width:100%; height:100%;
            position:absolute; top:0; left:0; z-index:99998;
        }
        
        form {
          width: 96%;
          margin: 0 auto;
          background-color: #f6f6f6;
          padding: 1rem;
          border-radius: 3px;
        }
        form:before {
          content: '';
          display: block;
        }
        form:after {
          content: '';
          display: table;
          clear: both;
        }
        /* Toggle Switch */
        input[type='checkbox'].switch-control {
          margin-left: -9999px;
          position: absolute;
        }
        input[type='checkbox'].switch-control ~ label {
          float: left;
          width: 100%;
          position: relative;
          line-height: 1.75rem;
          margin: 0.2rem 0;
          cursor: pointer;
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          user-select: none;
          text-align: left;
        }
        input[type='checkbox'].switch-control ~ label:before,
        input[type='checkbox'].switch-control ~ label:after {
          right: 0;
          position: absolute;
          display: block;
          top: 0;
          bottom: 0;
          content: ' ';
          width: 3.75rem;
          background-color: #C41230;
          border-radius: 3px;
          transition: all 100ms ease-in;
          background-size: 20px 9px;
        }
        input[type='checkbox'].switch-control ~ label:before {
          border: 1px solid #CFD4D8;
          content: 'OFF';
          text-align: left;
          text-indent: 2.15rem;
          line-height: 3;
          color: #fff;
          font-size: .5rem;
          font-weight: 900;
        }
        input[type='checkbox'].switch-control ~ label:after {
          right: 1.8rem;
          width: 1.75rem;
          top: 0.2rem;
          bottom: 0.25rem;
          margin-left: 0.2rem;
          background-color: #fff;
          border-radius: 2px;
          box-shadow: 0 0.1rem 0 rgba(0, 0, 0, 0.3);
          border: 1px solid #A7A9AC;
          content: '...';
          color: #5F6062;
          font-weight: 900;
          line-height: .7;
          text-align: center;
          font-size: .95rem;
        }
        input[type='checkbox'].switch-control:checked ~ label:before {
          background-color: #8BC300;
          content: 'ON';
          text-indent: .5rem;
          text-align: left;
        }
        input[type='checkbox'].switch-control:checked ~ label:after {
          right: .2rem;
        }

.float{
	position:fixed;
	width:48px;
	height:48px;
	bottom:20px;
	left:20px;
	background-color:#06C;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	z-index:9999;
}

.my-float{
	font-size:24px;
	margin-top:12px;
}

a.float + div.label-container {
  visibility: hidden;
  opacity: 0;
  transition: visibility 0s, opacity 0.5s ease;
}

a.float:hover + div.label-container{
  visibility: visible;
  opacity: 1;
}
    </style>
@endsection
@section('content')

    <a href="#" class="float">
        <i class="fa fa-search my-float"></i>
    </a>
    
            <div class="content-full">
               <div class="decoration decoration-margins"></div>
               <div class="content">
                   <h3 class="uppercase bolder center-text">@if($lang == "tr"){{ $cityInfo->name }}@else{{ $cityInfo->name_en }}@endif <span class="color-highlight">@lang('general.boxes')</span></h3>
               </div>
               
               <form>
                  <input type="checkbox" id="switch" value="1" class="switch-control" @if(isset($_GET['show']) && $_GET['show'] == 1) checked @endif>
                  <label for="switch" style="font-size:12px">@lang('general.show_only_unopened_boxes')</label>
                </form>
                
               <div class="blog-categories blog-categories-3 bottom-20" style="margin-top:20px">
                  @foreach($boxes as $val)
                    
                         @php
                             $json = json_decode($val->rooms);
                         @endphp
                         @if(!isset($json->{$city}))
                             <a href="#" class="open-box" data-number="{{ $val->id }}" id="box_{{ $val->id }}">
                                 <strong class="hidden-box_{{ $val->id }}"></strong>
                                 <em class="hidden-box_{{ $val->id }}" style="font-size:30px; top:30%"><i class="fa fa-lock" style="font-size:14px"></i><br />{{ $val->number }}</em>
                                 <span class="bg-red-dark opacity-10 hidden-box_{{ $val->id }}"></span>
                                 <img id="box_image_{{ $val->id }}_1" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/boxes/11.png') }}" class="preload-image responsive-image" alt="img">
                                 <img id="box_image_{{ $val->id }}_2" style="display:none" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/boxes/12.png') }}" class="preload-image responsive-image" alt="img">
                                 <img id="box_image_{{ $val->id }}_3" style="display:none" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/boxes/13.png') }}" class="preload-image responsive-image" alt="img">
                             </a>
                         @elseif($json->{$city} == 1)
                             <a href="#" class="opened-box">
                                 <img src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/boxes/13.png') }}" class="preload-image responsive-image" alt="img">
                             </a>
                         @else
                             <a href="#" class="opened-box">
                                 <img src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/boxes/12.png') }}" class="preload-image responsive-image" alt="img">
                             </a>
                         @endif
                  @endforeach
                  <div class="clear"></div>
                  
                  {{ $boxes->appends($_GET)->links('pagination.mobile') }}

               </div>
               
            </div>
            <div class="decoration decoration-margins"></div>
            <div id="overlay"></div>
@endsection
@section('scripts')
    <script>
        $( ".open-box" ).click(function() {

            //loadInterstitial();
            
            $('#overlay').fadeIn(300);
            
            @if(!Sentinel::check())
                swal("@lang('general.box_couldnt_be_opened')", "@lang('general.login_for_open_box')", "error");
            @else
            
            swal({
                title: "@lang('general.are_you_sure')",
                text: "@lang('general.box_you_selected_will_be_opened')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: "@lang('general.ok')",
                cancelButtonText: "@lang('general.cancel')",
                closeOnConfirm: false,
                closeOnCancel: false
             },
             function(isConfirm){
            
               if (isConfirm){
                 $.ajax({  
                            url: "../open-box",
                            type: "POST",  
                            data: {  
                                number: number,
                                city: '{{ $city }}',
                                cityName: '{{ $cityInfo->name }}'
                            },  
                            dataType: "json",  
                            success: function(data) {  
                                if(data.result == 0) {
                                    swal(data.status, data.status_2, "error");
                                } else if(data.result == 1) {
                                    $('#box_image_'+number+'_1').hide();
                                    $('#box_image_'+number+'_3').show();
                                    $('.hidden-box_'+number).hide();
                                    swal({
                                      title: data.status,
                                      text: data.status_2,
                                      imageUrl: 'https://cdn.dribbble.com/users/579758/screenshots/4222962/klsmc2ar0h6z575bdllfptt0zijhiisplgvucvyczu3zym1aoeujrzhb00eguzmw-_easy-resize.com_1x.jpg',
                                      imageWidth: 800,
                                      imageHeight: 600,
                                      imageAlt: 'Winner',
                                      animation: false
                                    });
                                    $('#ticket').html(data.ticket);
                                } else if(data.result == 3) {
                                    $('#box_image_'+number+'_1').hide();
                                    $('#box_image_'+number+'_2').show();
                                    $('.hidden-box_'+number).hide();
                                    $('#ticket').html(data.ticket);
                                    swal(data.status, data.status_2, "error");
                                } else {
                                    swal(data.status, data.status_2, "error");
                                }
                                
                            } 

                        });
                 
                 if("{{ session('browser', 'unknown') }}" == "android" && "{{ session('ads', 'yes') }}" == "yes" && Math.random() >= 0.35) {
                  android.showInterstitial();
                 }
                 
                 
                } else {
                    swal("@lang('general.cancelled')!", "@lang('general.do_you_want_to_open_another_box')", "error");
                  
                  if("{{ session('browser', 'unknown') }}" == "android" && "{{ session('ads', 'yes') }}" == "yes" && Math.random() >= 0.35) {
                    android.showInterstitial();
                   }
                   
                    e.preventDefault();
                }
                
             });
             
             var number = $(this).data("number");
                
            @endif
            $('#overlay').fadeOut(300);
            
        });
        
        $('#switch').change(function() {
            if(this.checked) {
                window.location.replace(window.location.href.split('?')[0]+'?show=1');
            } else {
                window.location.replace(window.location.href.split('?')[0]+'?show=0');
            }
        });
        
        $('.float').click(function() {
            swal({title: "{{ \Lang::get('general.search_box') }}",
              text: "{{ \Lang::get('general.enter_box_number') }}",
              type: 'input',
              inputType: "number",
              showCancelButton: true,
              cancelButtonText: "{{ \Lang::get('general.cancel') }}",
              closeOnConfirm: false,
              confirmButtonText: "{{ \Lang::get('general.ok') }}",
              animation: "slide-from-top"
              }, function(inputValue){
                  if(inputValue == "" || inputValue== null) {
                    //console.log("olmaz");   
                  } else {
                    window.location.replace(window.location.href.split('?')[0]+'?number='+inputValue);  
                  }
                  
              });
            });
    </script>
@endsection