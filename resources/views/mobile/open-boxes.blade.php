@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
    <table class="table-borders-dark">
        <tr>
            <th>@lang('general.city')</th>
            <th>@lang('general.number')</th>
            <th>@lang('general.earnings')</th>
            <th>@lang('general.date')</th>
        </tr>
        @forelse($openBoxes as $key => $val)
        <tr>
            <td>@if(session('lang') == "tr"){{ $val->name_en }}@else{{ $val->name_en }}@endif</td>
            <td>{{ $val->number }}</td>
            <td>@if($val->prize > 0)<strong>{{ number_format($val->prize, 2, ',', '.') }} LP</strong>@else - @endif</td>
            <td>{{ Carbon\Carbon::parse($val->created_at)->format('d M H:i') }}</td>
        </tr>
        @empty
        <tr>
            <td colspan ="4">@lang('general.you_havent_opened_any_boxes')</td>
        </tr>
        @endforelse
    </table>
@endsection
@section('scripts')
    
@endsection