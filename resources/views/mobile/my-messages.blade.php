@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
 			<div class="content">
               <div class="decoration"></div>
               @forelse($messages->reverse() as $key => $val)
               	@if($val->owner == 1)
	               <div class="speech-bubble speech-right color-black">
	                  {{ $val->message }}
	                  <br />
	                  <em class="speech-read">{{ Carbon\Carbon::parse($val->date)->format('d M H:i') }}</em>
	               </div>
               	   
               	   <div class="clear"></div>
	            @else
		            <div class="speech-bubble speech-left bg-highlight">
	                  {{ $val->message }}
	                  <br />
	                  <em class="speech-read">{{ Carbon\Carbon::parse($val->date)->format('d M H:i') }}</em>
	                </div>
	                <div class="clear"></div>
	            @endif

               @empty
               @endforelse
               <div style="height:40px"></div>
                {!! Form::open(['url'=>'support', 'method'=>'post', 'autocomplete'=>'off', 'style'=>'width:100%; position:fixed; bottom:0px; padding-right:10%'])  !!}
                	@if(Sentinel::getUser()->support < 2)
                	<div class="one-half" style="width:70%; margin-right:0px">
	               		<div class="input-simple-2 has-icon input-green bottom-15">
	               			<input type="text" name="message" placeholder="@lang('general.your_message')" style="width:100%; background-color:#fff" required>
	               		</div>
	               	</div>
	               	<div class="one-half last-column" style="width:30%">
	               		<button type="submit" class="uppercase ultrabold button button-green" style="width:100%; height:50px; line-height:46px">@lang('general.send')</button>
	               	</div>
	               	@else
	               	<div class="one-half" style="width:100%; margin-right:0px">
	               		<div class="input-simple-2 has-icon input-green bottom-15">
	               			<input type="text" name="message" placeholder="Mesaj göndermeniz engellenmiştir." style="width:100%; background-color:#fff" disabled>
	               		</div>
	               	</div>
	               	@endif
                {!! Form::close() !!}
            </div>
@endsection
@section('scripts')
    <script>
    	window.scrollTo(0, document.body.scrollHeight);
	</script>
@endsection