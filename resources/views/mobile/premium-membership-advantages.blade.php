@extends('layouts.mobile.main')
@section('styles')

@endsection
@section('content')
			<div class="single-store-slider owl-carousel owl-has-dots gallery store-product-slider">
               <a class="show-gallery" href="#"><img src="{{ asset('assets/mobile/images/memberships/'.$membership.'.png') }}" alt=""></a>
            </div>
			<div class="content">
               <div class="store-product">
                  <h2 class="store-product-title">{{ ucfirst($membership) }}</h2>
                  <span class="store-product-price"><!--<em>from <del>$1099</del></em>--><strong>{{ $price }}</strong></span>
               </div>               
                @if(Sentinel::getUser()->premium_membership == null || Sentinel::getUser()->premium_expiration < Carbon\Carbon::now())
                  @if(Sentinel::check() && Sentinel::getUser()->currency == "try")
                      <button class="button button-blue button-full button-s button-rounded uppercase ultrabold bottom-30" onClick="buyPremium('{{ $membership }}_tr', '{{ Sentinel::getUser()->id }}');">Buy Now</button>
                  @else
                      <button class="button button-blue button-full button-s button-rounded uppercase ultrabold bottom-30" onClick="buyPremium('{{ $membership }}_en', '{{ Sentinel::getUser()->id }}');">Buy Now</button>
                  @endif
                @endif
               <div class="decoration top-30"></div>
               <h4 class="bold bottom-15 color-highlight" style="text-align:center">{{ ucfirst($membership) }} @lang('general.membership_advantages')</h4>
               <table class="table-borders-dark store-product-table">
                  <tr>
                     <td class="bold color-black">@lang('general.prize_multiplier')</td>
                     <td>@lang('general.prize_multiplier_info')</td>
                  </tr>
                  <tr>
                     <td class="bold color-black">@lang('general.scratch_win')</td>
                     <td>@lang('general.scratch_win_info')</td>
                  </tr>
                  <tr>
                     <td class="bold color-black">@lang('general.video_ticket')</td>
                     <td>@lang('general.video_ticket_info')</td>
                  </tr>
                  <tr>
                     <td class="bold color-black">@lang('general.ad_free_use')</td>
                     <td>@lang('general.ad_free_use_info')</td>
                  </tr>
                  <tr>
                     <td class="bold color-black">@lang('general.live_support')</td>
                     <td>@lang('general.live_support_info')</td>
                  </tr>
                  <tr>
                     <td class="bold color-black">@lang('general.special_theme')</td>
                     <td>@lang('general.special_theme_info')</td>
                  </tr>
                  @if(Sentinel::check() && Sentinel::getUser()->country == "Turkey (+90)")
                  <tr>
                     <td class="bold color-black">Süre</td>
                     <td>Tüm premium üyeliklerin avantajlarından 24 saat boyunca yararlanabilirsiniz.</td>
                  </tr>
                  @endif
                  <!--
                  <tr>
                     <td class="bold color-black">Connectivity</td>
                     <td>WiFi <br> Bluetooth 4.0 </td>
                  </tr>
                  <tr>
                     <td class="bold color-black">Sensors</td>
                     <td>Accelerometer<br>Gyroscope<br>Heartrate Monitor </td>
                  </tr>
                  <tr>
                     <td class="bold color-black">Box Contents</td>
                     <td>1 Smart Watch<br>1 Charger Brick<br>1 Charger Cable <br>1 Extra Sports Band </td>
                  </tr>
                -->
               </table>
               @if(Sentinel::getUser()->premium_membership == null || Sentinel::getUser()->premium_expiration < Carbon\Carbon::now())
                   <div class="decoration"></div>
                   @if(Sentinel::check() && Sentinel::getUser()->currency == "try")
                        <button class="button button-blue button-full button-s button-rounded uppercase ultrabold bottom-30" onClick="buyPremium('{{ $membership }}_tr', '{{ Sentinel::getUser()->id }}');">Buy Now</button>
                    @else
                        <button class="button button-blue button-full button-s button-rounded uppercase ultrabold bottom-30" onClick="buyPremium('{{ $membership }}_en', '{{ Sentinel::getUser()->id }}');">Buy Now</button>
                    @endif
                @endif
            </div>
@endsection
@section('scripts')

@endsection