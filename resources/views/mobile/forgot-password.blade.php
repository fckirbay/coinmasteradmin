@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
            <a href="#" class="cover-back-button back-button"><i class="fa fa-chevron-left font-12 color-white"></i></a>
            <a href="index.html" class="cover-home-button"><i class="fa fa-home font-14 color-white"></i></a>
            <div class="cover-item cover-item-full">
               <div class="cover-content cover-content-center">
                  <div class="page-login content-boxed content-boxed-padding top-0 bottom-0">
                     <h3 class="uppercase ultrabold top-10 bottom-0" style="text-align:center">@Lang('general.reset_your_password')</h3>
                     
                     <br /><br />
                     <p class="smaller-text bottom-10" style="text-align:center; font-weight:bold; color:red">@Lang('general.you_should_write_your_phone_number')</p>
                     {!! Form::open(['url'=>'forgot-password', 'method'=>'post', 'class'=>'register-form outer-top-xs'])  !!}
                     <br />
                     <div class="page-login-field bottom-15">
                        <i class="fa fa-phone"></i>
                        <input type="number" name="phone" placeholder="@lang('general.phone_number')" maxlength="16" required>
                        <em>(@lang('general.required'))</em>
                     </div>
                     <div class="page-login-links bottom-10">
                        <a class="forgot float-right" href="{{ url('login') }}"><i class="fa fa-user float-right"></i>@lang('general.login')</a>
                        <a class="forgot float-left" href="{{ url('signup') }}"><i class="fa fa-user float-right"></i>@lang('general.signup')</a>
                        <!--<a class="create float-left" href="{{ url('forgot-password') }}"><i class="fa fa-eye"></i>@lang('general.forgot_password')</a>-->
                        <div class="clear"></div>
                     </div>
                     <button type="submit" class="button button-red button-full button-rounded button-s uppercase ultrabold bottom-10" id="recover">@lang('general.reset_my_password')</button>
                     {!! Form::close() !!}
                  </div>
               </div>
               <div class="cover-item cover-item-full" style="background-image:url(images/pictures_vertical/bg1.jpg);"></div>
               <div class="cover-overlay overlay bg-black opacity-80"></div>
            </div>
@endsection
@section('scripts')
   <script>
      $( "#recover" ).click(function() {
        android.showLoader();
        setTimeout(
        function() 
        {
         android.hideLoader();
        }, 4500);
      });

      
   </script>
@endsection