@extends('layouts.mobile.main')
@section('styles')

@endsection
@section('content')

<h3 class="uppercase bolder center-text" style="margin-top:50px"><span class="color-highlight">@lang('general.exchange')</span></h3>

<p class="center-text boxed-text-large bottom-30" style="margin-top:30px">
@lang('general.exchange_information')
</p>

<div class="content" style="margin-top:10px">
				{!! Form::open(['url'=>'exchange', 'method'=>'post', 'class'=>'register-form outer-top-xs'])  !!}
                   <div class="select-box select-box-1">
                        <em>@lang('general.choose_how_many_tickets_you_want_to_get')</em>
                        <select name="amount">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                   </div>
                   <button type="submit" class="button button-green" style="width:100%">@lang('general.change')</button>
               {!! Form::close() !!}
</div>
@endsection
@section('scripts')
  @if(session('browser') == "android")
  <script>
    if("{{ session('ads', 'yes') }}" == "yes" && Math.random() >= 0.35) {
        android.showInterstitial();
    }
  </script>
  @endif
@endsection