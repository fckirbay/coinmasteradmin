@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
            @forelse($products as $key => $val)
            <div class="store-slide-2">
               <a href="{{ url($val->slug) }}" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset($val->photo) }}" alt="{{ $val->title }}" style="max-height:70px">
               </a>
               <div class="store-slide-title">
                  <strong>{{ $val->title }}</strong>
                  <em class="color-gray-dark">{{ $val->subtitle }}</em>
               </div>
               <div class="store-slide-button">
                  <strong>@if($val->old_price > 0)
                            <del class="color-red-light font-10">{{ number_format($val->old_price, 2, ',', '.') }}₺</del>
                          @endif {{ number_format($val->price, 2, ',', '.') }}₺</strong>
                  <a href="{{ url($val->slug) }}"><i class="fa fa-search color-black"></i></a>
                  <a href="#"><i class="fa fa-heart color-highlight"></i></a>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            @empty
            <div style="text-align:center; margin-top:20px">Ürün bulunamadı!</div>
            @endforelse
            <div class="pagination" style="margin-top:20px">
                {{ $products->links('pagination.mobile') }}
                <!--<a href="#"><i class="fa fa-angle-left"></i></a>
                <a href="#" class="no-border">1</a>
                <a href="#" class="no-border">2</a>
                <a href="#" class="bg-blue-dark color-white">3</a>
                <a href="#" class="no-border">4</a>
                <a href="#" class="no-border">5</a>
                <a href="#"><i class="fa fa-angle-right"></i></a>-->
            </div>

@endsection
@section('scripts')
   
@endsection