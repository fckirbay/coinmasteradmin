@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
    <table class="table-borders-dark">
        <tr>
            <th>@lang('general.username')</th>
            <th>@lang('general.date')</th>
        </tr>
        @forelse($references as $key => $val)
        <tr>
            <td>{{ $val->first_name }}</td>
            <td>{{ Carbon\Carbon::parse($val->created_at)->format('d M H:i') }}</td>
        </tr>
        @empty
        <tr>
            <td colspan ="4">@lang('general.you_have_no_reference_members')</td>
        </tr>
        @endforelse
    </table>
@endsection
@section('scripts')
    
@endsection