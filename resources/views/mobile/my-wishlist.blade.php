@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
            @forelse($wishlist as $key => $val)
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset($val->photo) }}" alt="{{ $val->title }}">
               </a>
               <div class="store-slide-title">
                  <strong>{{ $val->title }}</strong>
                  <em class="color-gray-dark">{{ $val->subtitle }}</em>
               </div>
               <div class="store-slide-button">
                  <strong>{{ number_format($val->price, 2, ',', '.') }}₺</strong>
                  <a href="#"><i class="fa fa-times color-black"></i></a>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            @empty
            <p style="text-align:center">Favori listeniz boş!</p>
            @endforelse
@endsection
@section('scripts')
   
@endsection