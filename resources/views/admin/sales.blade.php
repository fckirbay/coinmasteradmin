@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Üyelik Satışları</h3>
                
              {!! Form::open(['url'=>'admin/sales', 'method' => 'get', 'style' => 'float:right'])  !!}
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="search" class="form-control pull-right" placeholder="Ara">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:10%">Kullanıcı ID</th>
                  <th class="orta" style="width:20%">Üyelik Paketi</th>
                  <th class="orta" style="width:10%">İşlem ID</th>
                  <th class="orta" style="width:5%">Tarih</th>
                  <th class="orta" style="width:5%">#</th>
                </tr>
                @forelse($sales as $key => $val)
                <tr>
                  <td><a href="{{ url('admin/user-management/user', $val->user_id) }}">{{ $val->username }}</a></td>
                  <td class="orta">{{ $val->membership_id }}</td>
                  <td class="orta">{{ $val->trans_id }}</td>
                  <td class="orta">{{ Carbon\Carbon::parse($val->expiry_date)->format('d/m/Y H:i:s') }}</td>
                  <td class="orta"><a href="{{ url('admin/cancel-sale', $val->id) }}" class="btn btn-danger btn-xs">İptal Et</a></td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            {{ $sales->links() }}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection