@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Kazananlar</h3>
              <div class="box-tools">
                <div class="row">
                  <div class="col-xs-12">
                  	{!! Form::open(['url'=>'admin/winners', 'method'=>'post', 'autocomplete' => 'off'])  !!}
                    	<button type="submit" class="btn btn-default"><i class="fa fa-plus"></i> Yeni Ekle</button>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:70%">Kullanıcı Adı</th>
                  <th class="orta" style="width:25%">Tutar</th>
                </tr>
                @forelse($winners as $key => $val)
                <tr>
                  <td>{{ $val->id }}</td>
                  <td>{{ $val->first_name }}</td>
                  <td>{{ $val->amount}}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="3" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            {{ $winners->links() }}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection