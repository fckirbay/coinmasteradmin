@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Slider <a href="#addnew" data-toggle="modal" class="btn btn-primary btn-xs">Yeni Ekle</a></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Ara">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:10%">Fotoğraf</th>
                  <th style="width:20%">Başlık 1</th>
                  <th style="width:20%">Başlık 2</th>
                  <th style="width:20%">Başlık 3</th>
                  <th style="width:10%">Buton Başlık</th>
                  <th style="width:10%">Buton Link</th>
                  <th style="width:10%">İşlem</th>
                </tr>
                @forelse($slides as $key => $val)
                <tr>
                  <td><img src="{{ asset($val->photo) }}" style="width:100%"/></td>
                  <td>{{ $val->title_1 }}</td>
                  <td>{{ $val->title_2 }}</td>
                  <td>{{ $val->title_3 }}</td>
                  <td>{{ $val->button_title }}</td>
                  <td>{{ $val->button_link }}</td>
                  <td>
                      {!! Form::open(['url'=>'admin/site-management/delete-slide', 'method'=>'post', 'class'=>'form-horizontal register-form outer-top-xs'])  !!}
                          <input type="hidden" name="slide_id" value="{{ $val->id }}"/>
                          <button type="submit" class="btn btn-danger btn-xs">Sil</button>
                      {!! Form::close() !!}
                      <a href="#edit_{{ $val->id }}" data-toggle="modal" class="btn btn-info btn-xs">Düzenle</button>
                  </td>
                </tr>
                
                
                
                
                
                <!-- Modals -->
                <div class="modal fade" id="edit_{{ $val->id }}" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Slide Düzenle</h4>
          </div>
          {!! Form::open(['url'=>'admin/site-management/edit-slide', 'method'=>'post', 'class'=>'form-horizontal register-form outer-top-xs'])  !!}
          <input type="hidden" name="slide_id" value="{{ $val->id }}"/>
          <div class="modal-body">
            <div class="form-group">
                <label class="control-label col-sm-3" for="email">Başlık 1</label>
                <div class="col-sm-9">
                  <input type="text" name="title_1" class="form-control" id="title_1" placeholder="Başlık 1" value="{{ $val->title_1 }}" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Başlık 2</label>
                <div class="col-sm-9">
                  <input type="text" name="title_2" class="form-control" id="title_2" placeholder="Başlık 2" value="{{ $val->title_2 }}" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Başlık 3</label>
                <div class="col-sm-9">
                  <input type="text" name="title_3" class="form-control" id="title_3" placeholder="Başlık 3" value="{{ $val->title_3 }}" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Buton Başlık</label>
                <div class="col-sm-9">
                  <input type="text" name="button_title" class="form-control" id="button_title" placeholder="Buton Başlık" value="{{ $val->button_baslik }}" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Buton Link</label>
                <div class="col-sm-9">
                  <input type="text" name="button_link" class="form-control" id="button_link" placeholder="Buton Link" value="{{ $val->button_link }}" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Sıra</label>
                <div class="col-sm-9">
                  <select class="form-control" name="parent_id">
                      <option value="1" @if($val->order == 1) selected @endif>1</option>
                      <option value="2" @if($val->order == 2) selected @endif>2</option>
                      <option value="3" @if($val->order == 3) selected @endif>3</option>
                      <option value="4" @if($val->order == 4) selected @endif>4</option>
                      <option value="5" @if($val->order == 5) selected @endif>5</option>
                      <option value="6" @if($val->order == 6) selected @endif>6</option>
                      <option value="7" @if($val->order == 7) selected @endif>7</option>
                      <option value="8" @if($val->order == 8) selected @endif>8</option>
                      <option value="9" @if($val->order == 9) selected @endif>9</option>
                      <option value="10" @if($val->order == 10) selected @endif>10</option>
                  </select>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
            <button type="submit" class="btn btn-primary">Kaydet</button>
          </div>
          {!! Form::close() !!}
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    
                @empty
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
    
    <div class="modal fade" id="addnew" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Slide Ekle</h4>
          </div>
          {!! Form::open(['url'=>'admin/site-management/new-slide', 'method'=>'post', 'class'=>'form-horizontal register-form outer-top-xs'])  !!}
          <div class="modal-body">
            <div class="form-group">
                <label class="control-label col-sm-3" for="email">Fotoğaf</label>
                <div class="col-sm-9">
                  <input type="text" name="ptoho" class="form-control" id="photo" placeholder="Fotoğraf">
                </div>
              </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="email">Başlık 1</label>
                <div class="col-sm-9">
                  <input type="text" name="title_1" class="form-control" id="title_1" placeholder="Başlık 1" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Başlık 2</label>
                <div class="col-sm-9">
                  <input type="text" name="title_2" class="form-control" id="title_2" placeholder="Başlık 2" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Başlık 3</label>
                <div class="col-sm-9">
                  <input type="text" name="title_3" class="form-control" id="title_3" placeholder="Başlık 3" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Buton Başlık</label>
                <div class="col-sm-9">
                  <input type="text" name="button_title" class="form-control" id="button_title" placeholder="Buton Başlık" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Buton Link</label>
                <div class="col-sm-9">
                  <input type="text" name="button_link" class="form-control" id="button_link" placeholder="Buton Link" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Sıra</label>
                <div class="col-sm-9">
                  <select class="form-control" name="parent_id">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
            <button type="submit" class="btn btn-primary">Kaydet</button>
          </div>
          {!! Form::close() !!}
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection