<!DOCTYPE HTML>
<html lang="en">
	<head>
    @include('partials.cardgame.head')
    @yield('styles')
    </head>
    <body class="theme-light" data-background="none" data-highlight="red2">
    	<div id="page">
			<div id="page-preloader">
				<div class="loader-main"><div class="preload-spinner border-highlight"></div></div>
			</div>
			@include('partials.cardgame.header')
			@include('partials.cardgame.footer')
        	@yield('content')
            <div class="page-bg" style="background-color: #fff">
                <div></div>
            </div>
        </div>
        <div class="menu-hider"></div>
        @include('partials.cardgame.scripts')
        @yield('scripts')
    </body>
</html>